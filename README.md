# Lab 7 - Working with the Stack ADT

## Getting Started
As usual, fork this repo into a private repo of your own. Clone your private repo and then create a develop branch in which you'll do your work. When you have finished your tasks, create a pull request and add Jim Daehn as a reviewer. After you have created the pull request, submit this lab through Blackboard. Be sure to use the Text submission to provide me with a *working* hyperlink to your pull request. Any and all members of your team must submit the lab on Blackboard that includes a Text Submission containing a working hyperlink pointing to appropriate pull request for your team.

Before you get started, make sure you can compile and run the original sources (Note -- the `$` is not to be typed; it is the command line prompt):

```
 $ g++ -std=c++11 *.h main.cpp PrecondViolatedExcep.cpp -o Runner
 
 $ ./Runner.exe
 Testing the Array-Based Stack:
 Empty: 1
 Pushing zero
 Pushing one
 Pushing two
 Pushing three
 Pushing four
 Pushing five
 Failed to push five onto the stack.
 Empty?: 0
 Loop 0
 peek1: four
 pop: 1
 Empty: 0
 Loop 1
 peek1: three
 pop: 1
 Empty: 0
 Loop 2
 peek1: two
 pop: 1
 Empty: 0
 Loop 3
 peek1: one
 pop: 1
 Empty: 0
 Loop 4
 peek1: zero
 pop: 1
 Empty: 1
 Empty: 1
 pop an empty stack:
 pop: 0
 peek into an empty stack:
 Precondition Violated Exception: peek() called with empty stack
```

## Part 1 - Resizable Stack
In the first part of this lab, modify the `ArrayStack` class so that it uses a resizable array to represent the stack items. Any time the stack becomes full, double the size of the array. Maintain the stack's bottom entry at the beginning of the array.

**Notes**:

* Be sure you list every member of your team in the header comments of any and all files you modify.
* Modify `main.cpp` to demonstrate this new resizing behavior.
* This task should include at least one commit; it is advisable to have more than one commit during your implementation of Part 1.
* Part 1 is Programming Problem 1 found on page 252.
* See section C2.5 Dynamic Allocation of Arrays beginning on page 130 for a review of how to create resizable arrays.
* You are free to add any needed attributes/operations to help support this functionality.

## Part 2 - User-defined Growth
Modify the `ArrayStack` to allow a user to define how much the array will resize itself when it becomes full. That is, instead of doubling the size of the array when it becomes full, you increase the size of the array by some positive integer _k_. Note: this should be done using an initializing constructor. If the client creates an `ArrayStack` using the default constructor, then the "doubling" strategy developed in Part 1 is employed. However, if the client creates an `ArrayStack` with an initializing constructor, i.e., one that supplies the value of _k_, then the resizing strategy is based on the value of _k_.

For example, an `ArrayStack` created as:

```
StackInterface<int>* stack = new ArrayStack<int>();
```

This stack will double its capacity when it becomes full. However, if an `ArrayStack` is created as:

```
StackInterface<int>* stack = new ArrayStack<int>(5);
```

This stack will resize itself by increasing its capacity to `MAX_STACK + 5` when it becomes full. (See `ArrayStack.h` for the declaration of `MAX_STACK`.)

**Notes**:

* Be sure you list every member of your team in the header comments of any and all files you modify.
* Modify `main.cpp` to demonstrate this new resizing behavior.
* This task should include at least one commit; it is advisable to have more than one commit during your implementation of Part 2.
* Part 2 is Programming Problem 5.
* See section C2.5 Dynamic Allocation of Arrays beginning on page 130 for a review of how to create resizable arrays.
* You are free to add any needed attributes/operations to help support this functionality.

## Grading

This lab will be graded on the following criteria:

1. Program correctness -- Successful implementation of Part 1.
1. Commits -- The team has at least one commit upon completion of Part 1.
1. Code quality -- The code is well documented, readable and uses consistent style for Part 1.
1. Program correctness -- Successful implemention of Part 2.
1. Commits -- The team has at least one commit upon completion of Part 2.
1. Code quality -- The code is well documented, readable and uses consistent style for Part 2.
1. Pull request -- The team implemented their work in a `develop` branch and created a pull request to merge their `develop` branch into their own `master` branch and invited me as a reviewer to that pull request.
1. On time submission -- The team submitted their assignment on time.
1. Blackboard semantics -- The team provided a navigable hyperlink to their pull request. The text submission also lists every member of the team.

Each of the above criteria are equally weighted.